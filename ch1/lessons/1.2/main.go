package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	s, sep := "", ""

	for idx, val := range os.Args {
		s += sep + "[" + strconv.Itoa(idx) + "] " + val
		sep = " "
	}

	fmt.Println(s)
}
