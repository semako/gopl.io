package main

import "testing"

func BenchmarkEchoLoop(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ArgsLoop()
	}
}

func BenchmarkEchoJoin(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ArgsJoin()
	}
}
