package main

import (
	"os"
	"strings"
)

func main() {
	ArgsLoop()
	ArgsJoin()
}

// ArgsLoop is
func ArgsLoop() {
	s, sep := "", ""

	for _, val := range os.Args {
		s += sep + val
		sep = " "
	}
}

// ArgsJoin is
func ArgsJoin() {
	strings.Join(os.Args, " ")
}
